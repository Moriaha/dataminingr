train_data <-read.csv('train.csv',stringsAsFactors=FALSE)
str(train_data)
test_data <-read.csv('test.csv',stringsAsFactors=FALSE)
str(test_data)

#keep only the relevant columns
train_keep <- subset( train_data, select = -c(PassengerId , Name , SibSp , Parch,Ticket,Fare,Cabin , Embarked) )
train_keep$FamilyMem <- train_data$Parch+train_data$SibSp+1
test_keep <- subset( test_data, select = -c(PassengerId , Name , SibSp , Parch,Ticket,Fare,Cabin , Embarked) )
test_keep$FamilyMem <- test_data$Parch+test_data$SibSp+1

library('tm')

#save columns as factor
train_keep$Survived <- as.factor(train_keep$Survived)
train_keep$Floor <- as.factor(train_keep$Pclass)
train_keep$Sex <- as.factor(train_keep$Sex)
train_keep$FamilyMem <- as.factor(train_keep$FamilyMem)
train_keep <- subset( train_keep, select = -c(Pclass) )
test_keep$Floor <- as.factor(test_keep$Pclass)
test_keep$Sex <- as.factor(test_keep$Sex)
test_keep$FamilyMem <- as.factor(test_keep$FamilyMem)
test_keep <- subset( test_keep, select = -c(Pclass) )

# complete missing data
library('mice')
tempDataPmm <- mice(train_keep,m=5,maxit=50,meth='pmm',seed=500)
tempDataPmm$imp$Age
train_keep <- complete(tempDataPmm,1)
tempDataPmm1 <- mice(test_keep,m=5,maxit=50,meth='pmm',seed=500)
tempDataPmm1$imp$Age
test_keep <- complete(tempDataPmm1,1)

#Fonction that convert age's column from numbers to categories
conv_cat <- function(x){
  
  if(x>60){
    x<- 2
  }
  else if(x>20){
    x<- 1
  }
  else{
    x<- 0
  }

  x <- factor(x, level =c(2,1,0), labels =c('Old','Adult', 'Child'))
}

train <- sapply(train_keep$Age, conv_cat)
train_keep$age_category <- train
train_keep$age_category <- as.factor(train_keep$age_category)
test <- sapply(test_keep$Age, conv_cat)
test_keep$age_category <- test
test_keep$age_category <- as.factor(test_keep$age_category)

# remove age column 
train_keep <- subset( train_keep, select = -c(Age) )
test_keep <- subset( test_keep, select = -c(Age) )


table(train_keep$age_category, train_keep$Survived)
table(train_keep$Sex, train_keep$Survived)
table(train_keep$FamilyMem, train_keep$Survived)
table(train_keep$Floor, train_keep$Survived)


####### data preparation end ###########


#spliting to training and testing data set
vec<-runif(891)
split<-vec>0.3 #bollean vec - 30% true and 70% false
split

#spliting the raw data
train_raw1 <- train_keep[split,] 
test_raw1 <- train_keep[!split,]
dim(train_raw1)
dim(test_raw1)


library(e1071)
library(SDMTools)

# Naive Bayes model
model1 <- naiveBayes(train_raw1[,-1] ,train_raw1$Survived) 
prediction_train <- predict(model1, train_raw1[,-1])
prediction_test <- predict(model1, test_raw1[,-1])
actual <- train_raw1$Survived
actual2 <- test_raw1$Survived
confusion.matrix(actual, prediction_train)
confusion.matrix(actual2, prediction_test)
